@extends('layouts.app')

@include('header')



<h3 style="margin-bottom:40px; margin-left: 20%; text-decoration: underline;">Tambah Sampah</h3>

<div  style="margin-left:30% ; margin-top:40px;" class="poin">

@foreach($antarsampah as $subject =>$sampah)
                    <form method="GET" action="/sampah/updatesampah" >
                        @csrf
                        <div class="form-group row" style="margin-top:70px;">
                            <label >Nama Sampah</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                                <input class="form-control" type="text" name="jenis_sampah" id="jenis_sampah" style="width:300px;" required value="<?php echo $sampah['JenisSampah'];?>">
                                
                            </div>
                        </div>

                        <div class="form-group row" ">
                            <label >Satuan</label>

                            <div class="col-sm-4"  style="margin-left:90px;">
                                <input class="form-control" type="text" name="satuan" id="satuan" style="width:300px;" required value="<?php echo $sampah['Satuan'];?>">
            
                                
                            </div>
                        </div>

                        <input type="hidden" name="ref" value="<?php echo $subject; ?>">
                        <div style="float:right; margin-right:50%;">

                        <button type="submit" class="btn btn-success " style="margin-right:20px">OK</button>
                        </div>
                    </form>

                    <div style="margin-left:220px;">
                        <form method="GET" action="/sampah/hapussampah" >
                        <input type="hidden" name="kunci" value="<?php echo $subject; ?>">
                        <button type="submit" class="btn btn-danger" style="margin-left:20px;">Hapus </button>
                        </form>

                       
                        </div> 


@endforeach
</div >

<div style="margin-top:200px; margin-left:5%;">

</div>

@include('footer')
