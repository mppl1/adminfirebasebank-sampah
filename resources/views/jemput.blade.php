@extends('layouts.app')

@include('header')

<h3 style="margin-bottom:40px;color:#268986 ;font-weight:bold; margin-left: 46%;">JEMPUT</h3>

<div class="btn-group btn-group-toggle" data-toggle="buttons" style="width:100%;" >
    <label class="btn " style="width:50%; border-radius:0; color:black;" >
      <input  type="radio" name="options" id="option1" autocomplete="off" checked> <a style="color:black;" href="#"> Isi Form</a>
    </label>
    <label class="btn "style="width:50%; border-radius:0;  ">
      <input  type="radio" name="options" id="option2" autocomplete="off"> <a style="color:black;" href="statusjemput"> Status</a>
    </label> 
  </div>




 <div  style="margin-left: 23%; margin-top:40px;">

 <form method="GET" action="/jemput/addjemput">
  @csrf

<div class="row">

<div  class="form-group row" style="margin-left:0.5px;">
                          <label class=" col-form-label">Jenis Sampah</label>
                            <div style="margin-left:15px;">  
                              <select class="form-control" id="" style="width:300px;" name="jenissampah" >
                              <option>Plastik</option>
                              <option>Botol Plastik</option>
                              <option>Botol Kaca</option>
                              <option>Sampah Elektronik</option>
                              <option>Besi</option>
                              <option>Alumunium</option>
                              <option>Kertas/Karton</option>
                            </select>   
                            </div>
  </div>



  <div class="form-group row" class="col-sm-2" style="margin-left:5%;"> 
  <label >Jumlah</label>
  <div class="col-sm-2">
    <input type="text"  class="form-control" name="berat" style="width:50px;">
  </div>
  </div>

  <div class="form-group row" class="col-sm-3" style="margin-left:5px;">
  <div class="col-sm-2" >
                            <div style="width:100px;">
                               
                                <select class="form-control" id="" name="satuan" >
                                  <option>Kg</option>
                                  <option>Pcs</option>
                                </select>
                            </div>
                        </div>
                        </div>
  </div>

  

  <div>
  <buttont style="margin-left:11%;"><img src="{{ ('ikon/plus.png') }}" alt="" style="width:20px; height:20px; "></button><br><br>
  </div>


  <div id="div" style="display:none;  margin-left:30px;" class="row">

<div class="row " style="margin-left:5%;" >
<div style="width:210px; " class="dropdown-item col-sm-5" href="#"  >  
<select class="form-control" id="" style="width:300px;" >
      <option>Plastik</option>
      <option>Botol Plastik</option>
      <option>Botol Kaca</option>
      <option>Sampah Elektronik</option>
      <option>Besi</option>
      <option>Alumunium</option>
      <option>Kertas/Karton</option>
    </select>   
</div>


<div class="form-group row " class="col-sm-3" style="margin-left:165px;"><label>Berat</label>
<div style="margin-left:13px;">
  <input type="text" class="form-control" id=""  style="width:50px;" name="Berat" >
</div>
  </div>
  </div>

  <div style="margin-left:35px;">
  <div style="width:100px;">
  <select class="form-control" id="" >
      <option>Kg</option>
      <option>Pcs</option>
    </select>
    </div>
</div>

  <div style="margin-left:3%;" >
  <buttonw ><img src="{{ ('ikon/cross.png') }}" alt="" style="width:20px; height:20px; "></button>
  </div>

</div> 

 
<div class="form-group row">
                            <label >Poin</label>

                            <div class="col-sm-4" style="margin-left:60px;">
                                <input type="text"  class="form-control" name="poin" style="width:300px;">
                            </div>
                        </div>

  <div class="form-group row"  >

            <label >Tanggal</label>

            <div class="col-sm-4"  style="margin-left:40px;">
            <input class="date form-control" type="text" name="tanggal" style="width:300px;" >
              </div>
  </div>
  

  <div class="form-group row "> 
  <div class="col-sm-1"> 
  <label> Lokasi Penjemputan</label> 
  </div>
    <div class="col-sm-4" style="margin-left:1%;">
      <input type="text" class="form-control" name="alamat" style="" >
    </div>
  </div>

  <div style="float:right; margin-right:23%; margin-top:5%;">
                        <button type="button" class="btn btn-danger" style="margin-right:20px;   ">Batal</button>
                        <button type="submit" class="btn btn-success" >OK </button>
                       
  
</form>



  <script type="text/javascript">

    $('.date').datepicker({  

       format: 'mm-dd-yyyy'

     });  

</script> 

<!-- input form -->
<script>
$(document).ready(function(){
  $("buttont").click(function(){
    $("#div").fadeIn("slow");
  });
});
</script>

<!-- close form -->
<script>
$(document).ready(function(){
  $("buttonw").click(function(){
    $("#div").fadeOut("slow");
  });
});
</script>


</div>

</div>


<div style="margin-top:200px; margin-left:5%;">
<a href="">Daftar Lokasi Pengantaran Sampah</a>
</div>


@include('footer')
