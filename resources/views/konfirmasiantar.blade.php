@extends('layouts.app')

@include('header2')



<h3 style="margin-bottom:40px; margin-left: 20%; text-decoration: underline;">Terima Sampah</h3>

<div class="btn-group btn-group-toggle" data-toggle="buttons" style="width:100%;" >
    <label class="btn " style="width:50%; border-radius:0; color:black;" >
      <input  type="radio" name="options" id="option1" autocomplete="off"> <a style="color:black;" href="terima"> Isi Form</a>
    </label>
    <label class="btn "style="width:50%; border-radius:0;  ">
      <input  type="radio" name="options" id="option2" autocomplete="off"  checked> <a style="color:black;" href="requestantar">Permintaan</a>
    </label> 
  </div>


<div style="margin-left:30%  ; margin-top:40px; margin-bottom:50px; " >
                     

                        <div class="form-group row">
                            <label >Email</label>

                            <div class="col-sm-4"  style="margin-left:60px;">
                            <p style="font-weight:bold;"> : <?php echo $email?></p>
                              
                            </div>
                        </div>

                        <div class="form-group row">
                            <label >No. Telepon </label>

                            <div class="col-sm-4"  style="margin-left:20px;">
                            <p style="font-weight:bold;"> : <?php echo $telp?></p>
                               
                                
                            </div>
                        </div>

</div>


@foreach($dataantar as $subject =>$data)
<div  style="margin-left:30%  ; margin-top:40px; margin-bottom:100px; " class="poin pointambah" >


                    <form method="GET" action="/konfirmasiantar/updateantar">
                        @csrf
                      
                        <div class="form-group row" style="display:none;">
                            <label >Email</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                                <input class="form-control" type="text" name="email" id="email" style="width:300px;" value="<?php echo $data['currentId'];?>" >
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label >Tanggal</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                            <p style="font-weight:bold;"> : <?php echo $data['tanggal'];?></p>
                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label >Status</label>

                            <div class="col-sm-4"  style="margin-left:50px;">
                            <p style="font-weight:bold;"> : <?php echo $data['status'];?></p>
                                
                            </div>
                        </div>

                        


                        <div class="row">

                        <div class="form-group row" style="margin-left:0.5px;">
                            <label class=" col-form-label">Jenis Sampah</label>

                            <div style="margin-left:15px;">
                              
                              <select class="form-control" id="jenis" style="width:300px;" name="jenissampah" >
                             
                            
                              <option><?php echo $data['jenisSampah'];?></option>
                            
                            </select>   
                            </div>
                        </div>


                        <div class="form-group row"class="col-sm-2" style="margin-left:4%;">
                            <label >Jumlah</label>

                            <div class="col-sm-2">
                                <input type="text"  class="form-control" name="berat" id="berat" style="width:50px;" value="<?php echo $data['berat'];?>">
                            </div>
                        </div>

                        <div class="form-group row" class="col-sm-3" style="margin-left:5px;">
                            
                            <div class="col-sm-2" >
                            <div style="width:100px;">
                               
                                <select class="form-control" id="" name="satuan" >
                                  <option>Kg</option>
                                  <option>Pcs</option>
                                </select>
                            </div>
                        </div>
                        </div>
                        </div>

  


  

                        
                        <div class="form-group row">
                            <label >Poin</label>

                            <div class="col-sm-4" style="margin-left:60px;">
                                <input type="text"  class="form-control" name="poin" id="poin" style="width:300px;" value="<?php echo $data['poin'];?>">
                            </div>
                        </div>

                

                        <div class="form-group row" style="display:none;">
                            <label >Poin Sebelumnya</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                                <input class="form-control" type="text"  id="poinuser" style="width:300px;"value="<?php echo $poin ?>">
                                
                            </div>
                        </div>

                        <div class="form-group row" style="display:none;">
                            <label >Poin Hasil</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                                <input class="form-control" type="text" id="hasil" name="poinuser" style="width:300px;">
                                
                            </div>
                        </div>

                        

                        <div class="form-group row" style="display:none;">
                            <label >Kode</label>

                            <div class="col-sm-4"  style="margin-left:40px;">
                                <input class="form-control" type="text" name="kode" style="width:300px;"value="<?php echo $subject?>">
                                
                            </div>
                        </div>


  

                        <script type="text/javascript">

                            $('.date').datepicker({  

                              format: 'mm-dd-yyyy'

                            });  

                        </script> 

                    

                        <div style="float:right; margin-right:25%; margin-top:1%;">
                        <!-- <button type="button" class="btn btn-danger" style="margin-right:20px;">Batal</button> -->
                        <button type="submit" class="btn btn-success">OK </button>
                       

                        </div> 
                    </form>


                   




                   

</div>

@endforeach
 


<script type="text/javascript">

$('.date').datepicker({  

   format: 'mm-dd-yyyy'

 });  

</script> 




<!-- Hitung Poin -->
<script type ="text/javascript">
    $(".poin").keyup(function(){
        var jenis = parseInt($("#jenis").val())
        var berat = parseInt($("#berat").val())
        
        var poin = jenis * berat;
        $("#poin").attr("value",poin)
        
        });
</script>

<script type ="text/javascript">
    $(".poin").keyup(function(){
        var poin = parseInt($("#poin").val())
        var poinuser = parseInt($("#poinuser").val())
        
        var hasil = poin + poinuser;
        $("#hasil").attr("value",hasil)
        
        });
</script>




<div style="margin-top:200px; margin-left:5%;">

</div>


@include('footer')
