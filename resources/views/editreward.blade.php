@extends('layouts.app')

@include('header')

<div class="row" style="margin-left: 0%;">

<div class="col-sm-9" >
<h3 style=" margin-left:3%;">Edit Reward</h3>

<div   style="margin-left:10% ; margin-top:40px;">
@foreach($reward as $subject => $reward)
<form method="GET" action="/reward/updatereward">
  <div class="form-group row" style="margin-top:10%;">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputnama" name="namaReward" required value="<?php echo $reward['namaReward'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Jenis</label>
    <div class="col-sm-7">
    <input list="reward" class="form-control" name="jenisReward" required value="<?php echo $reward['jenisReward'];?>">
      <datalist id="reward">
              <option>Voucher</option>
              <option>Barang</option>
      </datalist>
    </div>
    <!-- <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputlokasi" name="jenisReward" required value="<?php echo $reward['jenisReward'];?>">
    </div> -->
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Point</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputdeskripsi" name="pointReward" required value=" <?php echo $reward['pointReward'];?>">
    </div>
  </div>

  <input type="hidden" name="ref" value="<?php echo $subject; ?>">
  <div style="float:right; margin-right:25%;">
    <button type="submit" class="btn btn-success " style="margin-right:20px">OK</button>
  </div>

</form>

  <div style="margin-left:400px;">
    <form method="GET" action="/reward/hapusreward" >
      <input type="hidden" name="kunci" value="<?php echo $subject; ?>">
      <button type="submit" class="btn btn-danger" style="margin-left:20px;">Hapus </button>
    </form>
  </div> 
@endforeach
</div>
<script type="text/javascript">
$('.date').datepicker({  
  format: 'mm-dd-yyyy'
});  
</script> 

</div>

<!-- <div class="col-sm-3" style= "bottom:-5px; left:-100px">
<img src ="{{ ('#') }}" style="width:150px">
<button type="button" class="btn btn-success" style="margin-left:-120px; margin-top:200px;">Tambah</button>
<div class="form-group" style="margin-top:5%;">
      <input type="file" class="form-control-file border" name="file">
    </div> -->
</div>

</div>



@include('footer')