@extends('layouts.app')

@include('header')

<div class="row" style="margin-left: 0%;">

<div class="col-sm-9" >
<h3 style=" margin-left:3%;">Edit Event</h3>

<div   style="margin-left:10% ; margin-top:40px;">
@foreach($event as $subject => $kegiatan)

<form method="GET" action="/event/updateevent">
<div class="form-group row" style="margin-top:10%;">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputnama" name="nama" required value="<?php echo $kegiatan['namaEvent'];?>">
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tanggal</label>
    <div class="col-sm-7">
      <input type="text" class="date form-control" required id="inputtanggal" name="tanggal" required value="<?php echo $kegiatan['waktuEvent'];?>">
     
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Lokasi</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputlokasi" name="lokasi" required value="<?php echo $kegiatan['tempatEvent'];?>">
      
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Deskripsi</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputdeskripsi" name="deskripsi" required value=" <?php echo $kegiatan['descEvent'];?>">
    </div>
  </div>

  <input type="hidden" name="ref" value="<?php echo $subject; ?>">
  <div style="float:right; margin-right:25%;">

<button type="submit" class="btn btn-success " style="margin-right:20px">OK</button>
</div>
 
 

 
</form>

  <div style="margin-left:400px;">
    <form method="GET" action="/event/hapusevent" >
      <input type="hidden" name="kunci" value="<?php echo $subject; ?>">
      <button type="submit" class="btn btn-danger" style="margin-left:20px;">Hapus </button>
    </form>
  </div> 

@endforeach
</div>
<script type="text/javascript">

$('.date').datepicker({  

   format: 'mm-dd-yyyy'

 });  

</script> 




</div>

<!-- <div class="col-sm-3" style= "bottom:-5px; left:-100px">
<img src ="{{ ('#') }}" style="width:150px">
<button type="button" class="btn btn-success" style="margin-left:-120px; margin-top:200px;">Tambah</button>
<!-- <div class="form-group" style="margin-top:5%;">
      <input type="file" class="form-control-file border" name="file">
    </div> -->
</div> -->

</div>



@include('footer')