@extends('layouts.app')

@include('header')

<div class="row" style="margin-left: 0%;">

<div class="col-sm-9" >
<h3 style=" margin-left:50%; margin-bottom:30px;">Tambah Event</h3>


<form method="GET" action=" /tambahevent/addtambahevent">
 @csrf

<div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Nama</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputnama" name="nama" required >
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Tanggal</label>
    <div class="col-sm-7">
      <input type="text" class="date form-control" required id="inputtanggal" name="tanggal" required >
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Lokasi</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputlokasi" name="lokasi" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="inputPassword" class="col-sm-2 col-form-label">Deskripsi</label>
    <div class="col-sm-7">
      <input type="text" class="form-control" required id="inputdeskripsi" name="deskripsi" required>
    </div>
  </div>

  <div style="float:right; margin-right:25%;">
<button type="button" class="btn btn-danger" style="margin-right:30px;">Batal</button>
<button type="submit" class="btn btn-success " style="margin-right:20px">OK</button>
</div>
 
 

 
</form>

<script type="text/javascript">

$('.date').datepicker({  

   format: 'mm-dd-yyyy'

 });  

</script> 




</div>

<div class="col-sm-3" style= "bottom:-5px; left:-100px">
<img src ="{{ ('#') }}" style="width:150px">
<button type="button" class="btn btn-success" style="margin-left:-120px; margin-top:200px;">Tambah</button>
<!-- <div class="form-group" style="margin-top:5%;">
      <input type="file" class="form-control-file border" name="file">
    </div> -->
</div>

</div>



@include('footer')