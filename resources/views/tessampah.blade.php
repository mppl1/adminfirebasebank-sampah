@extends('layouts.app')

@include('header')

<h3 style="margin-bottom:40px; margin-left: 20%; text-decoration: underline;">Data Sampah</h3>
<div style="margin-left:50px;"><a href="sampah">Tambah Sampah</a></div>
<div style="margin-left:50px;"><a href="editsampah">Edit Sampah</a></div>
<div  style="margin-left: 30px;margin-right:30px; margin-top:40px;">
<div >    

                <div class="card-body">
                <table class="table table-bordered">
                <thead>
                    <tr>
                    <th>No</th>
                   
                    <th>Jenis Sampah</th>
                  
                    <th>Satuan</th>
                  
                  
                    </tr>

                  </thead>

                    
                    <tbody>
                    <?php
                        $no = 1;
                    ?>
                    @foreach($antarsampah as $subject =>$sampah)
                    <tr>
                      <td><?php echo $no;?></td>
                      <td><?php echo $sampah['JenisSampah'];?></td>
                      <td><?php echo $sampah['Satuan'];?></td>
                  
                      <!-- <td><a href="editsampah">Edit</a></td> -->
                      <!-- <td>
                      
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                                              Launch demo modal
                                            </button>
                      </td> -->
                                            <!-- Modal -->
                                            <div class="modal " id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                              <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                  <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                      <span aria-hidden="true">&times;</span>
                                                    </button>
                                                  </div>
                                                  <div class="modal-body">
                                                  <form method="GET" action="/sampah/updatesampah">
                                                    <label for="">Jenis Sampah</label>
                                                    <input class="form-control" type="text" name="jenis_sampah" id="jenis_sampah" style="width:300px;" required value="<?php echo $sampah['JenisSampah'];?>">
                                                   
                                                    <label for="">Satuan </label>
                                                    <input class="form-control" type="text" name="satuan" id="satuan" style="width:300px;" required value="<?php echo $sampah['Satuan'];?>">

                                                    <input type="text" name="ref" value="<?php echo $subject; ?>">

                                                  
                                                  <button type="submit" class="btn btn-success">OK </button>
                                                  </form>
                                                  </div>
                                                  
                                                </div>
                                              </div>
                                            </div>
                      
                    
                    </tr>
                    <?php
                        $no++;
                    ?>
                    @endforeach
                   
                    </tbody>
                               
                   
                  
                    </table>
                </div>
            </div>
</div>




@include('footer')
