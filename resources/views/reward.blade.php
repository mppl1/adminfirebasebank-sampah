@extends('layouts.app')

@include('header')


<h3 style="margin-bottom:40px; margin-left: 20%; text-decoration: underline;">Reward</h3>

<div class="btn-group btn-group-toggle" data-toggle="buttons" style="width:100%;" >
    <label class="btn " style="width:33%; border-radius:0; color:black;" >
      <input  type="radio" name="options" id="option1" autocomplete="off" checked> <a style="color:black;" href="terima">Daftar</a>
    </label>
    <label class="btn "style="width:33%; border-radius:0;  ">
      <input  type="radio" name="options" id="option2" autocomplete="off"> <a style="color:black;" href="requestantar">Permintaan</a>
    </label>
    <label class="btn "style="width:33%; border-radius:0;  ">
      <input  type="radio" name="options" id="option3" autocomplete="off"> <a style="color:black;" href="requestantar">Status</a>
    </label> 
  </div>

  <div style="margin-left:50px; margin-top:20px;"><a href="tambahreward">Tambah Reward</a></div>
  <div style="margin-left:50px;"><a href="editreward">Edit Reward</a></div>


@foreach($all_reward as $subject)
<div class="card" style="margin-top:5%; width:90%;">
<div class="row">
  <div class="card1-mb-3 text-center" style="width: 100rem; height: 20rem;">
    <div class="col-md-3 float-right"> 
      <h2 style="color:black">{{$subject['namaReward']}}</h2>
      <span class="badge badge-primary"><h5>{{$subject['jenisReward']}}</h5></span>
      <span class="badge badge-primary"><h5>{{$subject['pointReward']}} </h5></span>
  </div>
  <div class="d-inline p-2 bg-white"><img style="width: 40rem ; margin-top:20px;  height: 15rem" src= "{{$subject['urlreward']}}"></div>
  </div>
</div>
</div>

@endforeach


@include('footer')