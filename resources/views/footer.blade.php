@extends('layouts.app')

<footer class="footer-area section-gap" style="background-color:#295C58; margin-top:2%;" >
     
        <div class="row" style="color:white; margin-left:10%;">
          <div class="col-lg-3 col-md-6 col-sm-6">
            <div class="single-footer-widget">
              <div style="margin-left:5%;">
              <div >
              <img src="{{ ('ikon/pin.png') }} " style="width:35px; height:35px; margin-left:15%;">
              <p style="margin-top:5%;">Institut Teknologi Del</p>
              </div>

              </div>
            </div>
          </div>

          <div class="col-lg-3  col-md-6 col-sm-6">
            <div class="single-footer-widget">
          
              <div style="margin-left:5%;">
              <div >
              <img src="{{ ('ikon/phone.png') }} "style="width:35px; height:35px; margin-left:5%;">
              <p style="margin-top:5%;">083333333</p>
              </div>
            
              </div>
            </div>
          </div>	

          <div class="col-lg-3  col-md-6 col-sm-6">
            <div class="single-footer-widget">
             
              <div style="margin-left:5%;">
              <div >
              <img src="{{ ('ikon/www.png') }} "style="width:35px; height:35px; margin-left:5%;">
              <p style="margin-top:5%;"> http://.com</p>
              </div>
              </div>
            </div>
          </div>	


          <div class="col-lg-3 col-md-6 col-sm-6 social-widget">
            <div class="single-footer-widget">
              
             <div class="row">
              <img src="{{ ('ikon/instagram.png') }} " style="width:20px; height:20px; margin-right:20px;">
              <p>@Sipahias</p>
              </div>

              <div class="row">
              <img src="{{ ('ikon/facebook.png') }} " style="width:20px; height:20px; margin-right:20px;">
              <p>Sipahias Bank Sampah</p>
              </div>

              <div class="row">
              <img src="{{ ('ikon/twitter.png') }} " style="width:20px; height:20px; margin-right:20px;">
              <p>@Sipahias</p>
              </div>
            </div>
          </div>							
        </div>

        

        <div style="margin-left:42%; margin-top:5%; padding-bottom:3%;">
        <div class="row">
        <img src="{{ ('ikon/copyright.png') }} " style="width:20px; height:20px; margin-right:20px;">
        <p style="color:white;">MPPL, All right Reserved</p>
        </div>
        </div>
    
    </footer>


