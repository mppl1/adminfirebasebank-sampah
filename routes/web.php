<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('customers', 'customers');

Route::get('/edit', 'BaruController@edit');

Route::get('/tukarpoin', 'BaruController@tukarpoin');

Route::get('/dashboard', 'BaruController@dashboard');

// Route::get('/antar', 'BaruController@antar');

Route::get('/jemput', 'BaruController@jemput');

Route::get('/modal', 'BaruController@modal');

Route::get('/your', 'BaruController@your');

Route::get('/formedit', 'BaruController@formedit');



Route::get('/TukarPoin', 'BaruController@TukarPoin');

Route::get('/firebase', 'FirebaseController@index');





//tes kalkulator
Route::get('/kalkulator','BaruController@kalkulator');


// Ini Coba firebase Buat Ngambil data
// Route::get('/statusantar', 'AntarsampahController@index')->name('subject');

Route::get('/antar', 'AntarsampahController@antar');

Route::get('/cariantar', 'AntarsampahController@cari');

Route::get('/cariantar/cari', 'AntarsampahController@cariantar');

Route::get('/antar/addantar', 'AntarsampahController@addantar')->name('antar_add');

//Jemput Sampah
Route::get('/statusjemput', 'JemputsampahController@index')->name('subject');

Route::get('/pelanggan', 'PelangganController@pelanggan')->name('pelanggan');

Route::get('/pelanggan/datapelanggan', 'PelangganController@datapelanggan')->name('pelanggan');


// Route::get('/pelanggan/updateprofil', 'PelangganController@updateprofil')->name('pelanggan');


Route::get('/antar', 'AntarsampahController@antar');



Route::get('/jemput/addjemput', 'JemputsampahController@addjemput')->name('jemput_add');

Route::post('/jemput/updatejemput', 'JemputsampahController@updatejemput')->name('jemput_update');

// konfirmasi Jemput
 Route::get('/konfirmasijemput', 'JemputsampahController@index2')->name('subject');

// Route::get('/konfirmasijemput', 'JemputsampahController@konfirmasijemput');

// Terima Sampah
// Route::get('/statusjemput', 'JemputsampahController@index')->name('subject');

Route::get('/terima', 'TerimasampahController@terima');

Route::get('/terima/addterima', 'TerimasampahController@addterima')->name('terima_add');

//Tambah Sampah

Route::get('/sampah', 'SampahController@sampah');

Route::get('/datasampah', 'SampahController@datasampah');

Route::get('/tessampah', 'SampahController@tessampah');
Route::get('/editsampah', 'SampahController@editsampah');
Route::get('/sampah/updatesampah', 'SampahController@updatesampah');

Route::get('/sampah/hapussampah', 'SampahController@hapussampah');


Route::get('/sampah/addsampah', 'SampahController@addsampah')->name('sampah_add');




//Autentikasi
Route::get('firebase', 'FirebaseController@pushSet');
Route::middleware('auth')->get('/','DashboardController@index');
Route::get('login','LoginController@login')->name('login');
Route::get('/','LoginController@login')->name('login');
Route::get('register','LoginController@register')->name('register');
Route::post('login','LoginController@loginAuth')->name('login');
Route::post('register','LoginController@registerAuth')->name('register');
Route::get('logout','LogoutController@logout')->name('logout');
Route::get('editprofil','EditProfilController@editprofil')->name('edit');
Route::post('editprofil','EditProfilController@editprofilAuth')->name('edit');



// Konfirmasi
Route::get('/konfirmasi', 'KonfirmasiController@konfirmasi');

// Route::get('/statusantar', 'KonfirmasiController@index');


// Proses Konfirmasi antar
Route::get('/requestantar', 'AntarsampahController@requestantar');

Route::get('/requestantar/dataantar', 'AntarsampahController@dataantar');


Route::get('/konfirmasiantar', 'AntarsampahController@konfirmasiantar');

// Proses Konfirmasi jemput
Route::get('/requestjemput', 'JemputsampahController@requestjemput');

Route::get('/requesjemput/datajemput', 'JemputsampahController@datajemput');


Route::get('/konfirmasijemput', 'JemputsampahController@konfirmasijemput');




Route::get('/konfirmasiantar/updateantar', 'AntarsampahController@updateantar');
Route::get('/konfirmasijemput/updatejemput', 'JemputsampahController@updatejemput');


Route::get('/poin', 'SampahController@poin');




//Tambah event
Route::get('/event', 'EventController@event');

Route::get('/editevent', 'EventController@editevent');

Route::get('/event/updateevent', 'EventController@updateevent');
Route::get('/event/hapusevent', 'EventController@hapusevent');


Route::get('/tambahevent', 'EventController@tambahevent');

Route::get('/tambahevent/addtambahevent', 'EventController@addtambahevent')->name('tambahevent_add');

//Reward
Route::get('/reward', 'RewardController@reward');
Route::get('/tambahreward', 'RewardController@tambahreward');

Route::get('/tambahreward/addtambahreward', 'RewardController@addtambahreward')->name('tambahreward_add');

Route::get('/editreward', 'RewardController@editreward');

Route::get('/reward/updatereward', 'RewardController@updatereward');
Route::get('/reward/hapusreward', 'RewardController@hapusreward');



Route::get('/riwayat', 'AntarSampahController@riwayat');
Route::get('/riwayatjemput', 'JemputSampahController@riwayatjemput');
Route::get('/riwayatantar/dataantar', 'AntarSampahController@riwayatdataantar');
Route::get('riwayatjemput/datajemput', 'JemputsampahController@riwayatdatajemput');

