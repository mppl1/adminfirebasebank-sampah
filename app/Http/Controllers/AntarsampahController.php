<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;

class AntarsampahController extends Controller
{

    // protected $cek = email;
    public function cariantar(Request $request){
        $this->email = $request->email;
        // $email = $this->email;
        // return redirect()->action('AntarsampahController@index');
        // return view('StatusAntar',compact('email'));
         return $this->email;
    }

   

    public function addantar(Request $request){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
   
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("AntarSampah");

        $jenissampah = $request->jenissampah;
        $berat  = $request->berat;
        $satuan = $request->satuan;
        $tanggal = $request->tanggal;
        $poin = $request->poin;
        $status = $request->status;

        $key = $ref->push()->getKey();

        $ref->getChild($key)->set([
            'JenisSampah'=>$jenissampah,
            'Berat'=>$berat,
            'Satuan'=>$satuan,
            'Tanggal'=>$tanggal,
            'Poin'=>$poin,
            // 'Status'=>$status
            'Status'=>'Sedang diproses'
            
        ]);

        $subjects = $ref->getValue();

        foreach($subjects as $subject){
            $all_subject[] = $subject;
        }



        return view('antar',compact('all_subject'));

    }


    public function antar(){
        return view('antar');
    }
    
    public function cari(){
        return view('cari');
    }

    

    public function requestantar(){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("AntarSampah");
        $antarsampah = $ref->getValue();

            // return json_encode($all_subject);
          return view('requestantar',compact('antarsampah'));
          

    }




    public function dataantar(Request $request){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $kode = $request->ref;

        $database = $firebase->getDatabase();
        $db = $firebase->getDatabase();
        $jalur = "AntarSampah/{$kode}";

        // $ref =  $database->getReference("$jalur")->orderByChild('Status')->equalTo("Sedang diproses");
        $ref =  $database->getReference("$jalur");;
        // $stat = $database->getReference("AntarSampah/{$kode}/Status");
        
        $dataantar = $ref->getValue();

        // $dataantar = $db->getReference("AntarSampahWeb/{$kode}")
        // // order the reference's children by the values in the field 'height'
        // ->orderByValue()
        // // limits the result to the last 10 children (in this case: the 10 tallest persons)
        // ->limitToLast(1)
        // ->getSnapshot();
        // $proses = $dataantar->orderByValue()->limitToLast(1)->getSnapshot();
    


        $ambilpoin =  $database->getReference("Users/{$kode}/point");
        $poin = $ambilpoin->getValue();

        $ambiltelp =  $database->getReference("Users/{$kode}/no_telp");
        $telp = $ambiltelp->getValue();


        $ambilemail =  $database->getReference("Users/{$kode}/email");
        $email= $ambilemail->getValue();


            // return json_encode($dataantar);
        //    return view('statusantar',compact('proses'));
        
        return view('konfirmasiantar',compact('dataantar','poin','telp','email'));
    }



    
    public function updateantar(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();
        $database = $firebase->getDatabase();
        $email = $request->email;
   
        $jenissampah = $request->jenissampah;
        $berat  = $request->berat;
        $satuan = $request->satuan;
        $tanggal = $request->tanggal;
        $poin = $request->poin;
       
        $kode = $request->kode;

        $poinuser = $request->poinuser;

        $updatepoin = [
            'point'=>$poinuser
        ];
     
        $pushPoin = $database->getReference("Users/{$email}")->update($updatepoin);

        $data = [
            'jenisSampah'=>$jenissampah,
            'berat'=>$berat,
            'satuan'=>$satuan,
            // 'Tanggal'=>$tanggal,
            'poin'=>$poin,
            'currentId'=>$email,
            'status'=>'Berhasil'
            
        ];

        $pushData = $database->getReference("AntarSampah/{$email}/{$kode}")->update($data);


        return redirect()->action('AntarsampahController@requestantar');

    }


    public function riwayat(){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("AntarSampah");
        $antarsampah = $ref->getValue();

        $ref2 =  $database->getReference("JemputSampah");
        $jemputsampah = $ref2->getValue();

            // return json_encode($all_subject);
          return view('riwayatantar',compact('antarsampah','jemputsampah'));
          

    }


    public function riwayatdataantar(Request $request){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $kode = $request->ref;

        $database = $firebase->getDatabase();
        $db = $firebase->getDatabase();
        $jalur = "AntarSampah/{$kode}";

        // $ref =  $database->getReference("$jalur")->orderByChild('Status')->equalTo("Sedang diproses");
        $ref =  $database->getReference("$jalur");;
        // $stat = $database->getReference("AntarSampah/{$kode}/Status");
        
        $dataantar = $ref->getValue();
        
       


        $ambilpoin =  $database->getReference("Users/{$kode}/point");
        $poin = $ambilpoin->getValue();

        $ambiltelp =  $database->getReference("Users/{$kode}/no_telp");
        $telp = $ambiltelp->getValue();


        $ambilemail =  $database->getReference("Users/{$kode}/email");
        $email= $ambilemail->getValue();


            // return json_encode($dataantar);
        //    return view('statusantar',compact('proses'));
        
        return view('riwayatantardata',compact('dataantar','poin','telp','email'));
    }

    

    
}
