<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BaruController extends Controller
{
    
    public function antar()
    {
        return view('antar');
    }

    public function jemput()
    {
        return view('jemput');
    }

    public function your()
    {
        return view('your');
    }

    public function formedit()
    {
        return view('formedit');
    }

    public function StatusAntar()
    {
        
        return view('statusAntar');
    }
    
    public function TukarPoin()
    {
        return view('TukarPoin');
    }

    public function edit()
    {
        return view('edit');
    }

    public function modal()
    {
        return view('modal');
    }

    public function kalkulator(){
        return view('kalkulator');
    }

    public function konfirmasijemput(){
        return view('konfirmasijemput');
    }

    
}
