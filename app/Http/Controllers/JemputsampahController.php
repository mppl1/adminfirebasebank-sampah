<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class JemputsampahController extends Controller
{
    public function index(){    
         // $this->email =  $this->cariantar();
         $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
         $firebase = (new Factory)
         ->withServiceAccount($serviceAccount)
         // The following line is optional if the project id in your credentials file
         // is identical to the subdomain of your Firebase project. If you need it,
         // make sure to replace the URL with the URL of your project.
         ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
         ->create();
 
         $database = $firebase->getDatabase();
 
         $ref =  $database->getReference("JemputSampah")->orderByKey()
         ->getSnapshot();
         $antarsampah = $ref->getValue();
         // $coba = $antarsampah->getValue();
         foreach($antarsampah as $subject){
             $all_subject[] = $subject;
         }
 
         // $data = $all_subject->getValue();
 
         foreach($subject as $dat){
             $all_data[] = $dat;
         }
 
         //    return json_encode($all_data);
           return view('StatusJemput',compact('all_data'));
 
     }


    public function indextambah(){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();

            $database = $firebase->getDatabase();
        
            $ref = $database->getReference('JemputSampah');

            $key = $ref->push()->getKey();
            $ref->getChild($key)->set([
                'JenisSampah'=>'Sampah Mania3',
                'Berat'=>'200',
                'Satuan'=>'Kg',
                'Tanggal'=>'2020/03/22',
                'Poin'=>'10500',
                'Alamat'=>'GD 9',
                'Status'=>'Sedang diproses'
                
            
            ]);

            return $key;
    

    }

    public function addjemput(Request $request){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
   
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("JemputSampah");

        $jenissampah = $request->jenissampah;
        $berat  = $request->berat;
        $satuan = $request->satuan;
        $tanggal = $request->tanggal;
        $poin = $request->poin;
        $alamat = $request->alamat;
        $status = $request->status;

        $key = $ref->push()->getKey();

        $ref->getChild($key)->set([
            'JenisSampah'=>$jenissampah,
            'Berat'=>$berat,
            'Satuan'=>$satuan,
            'Tanggal'=>$tanggal,
            'Poin'=>$poin,
            // 'Status'=>$status
            'Alamat'=>$alamat,
            'Status'=>'Sedang diproses'
            
        ]);

        $subjects = $ref->getValue();

        foreach($subjects as $subject){
            $all_subject[] = $subject;
        }


        return view('jemput',compact('all_subject'));

    }


    public function index2(){    
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
   
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("JemputSampah/senin@gmail_com");

        $antarsampah = $ref->getValue();

        foreach($antarsampah as $subject){
            $all_subject[] = $subject;
        }

        // return json_encode($all_subject);
        return view('konfirmasijemput',compact('all_subject'));

    }
    public function jemput(){
        return view('jemput');
    }

    public function konfirmasijemput(){
        return view('konfirmasijemput');
    }



    


    public function requestjemput(){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("JemputSampah");
        $jemputsampah = $ref->getValue();

            // return json_encode($all_subject);
          return view('requestjemput',compact('jemputsampah'));
          

    }


    public function datajemput(Request $request){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $kode = $request->ref;

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("JemputSampah/{$kode}");
        $datajemput = $ref->getValue();


        $ambilpoin =  $database->getReference("Users/{$kode}/point");
        $poin = $ambilpoin->getValue();

        $ambiltelp =  $database->getReference("Users/{$kode}/no_telp");
        $telp = $ambiltelp->getValue();


        $ambilemail =  $database->getReference("Users/{$kode}/email");
        $email= $ambilemail->getValue();

        
        return view('konfirmasijemput',compact('datajemput','poin','telp','email'));
    }



    public function updatejemput(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();
        $database = $firebase->getDatabase();
        $email = $request->email;
   
        $jenissampah = $request->jenissampah;
        $berat  = $request->berat;
        $satuan = $request->satuan;
        $tanggal = $request->tanggal;
        $poin = $request->poin;
        $alamat = $request->alamat;
       
        $kode = $request->kode;

        $poinuser = $request->poinuser;

        $updatepoin = [
            'point'=>$poinuser
        ];
     
        $pushPoin = $database->getReference("Users/{$email}")->update($updatepoin);
     

        $data = [
            'JenisSampah'=>$jenissampah,
            'Berat'=>$berat,
            'Satuan'=>$satuan,
            // 'Tanggal'=>$tanggal,
            'Poin'=>$poin,
            'currentId'=>$email,
            'Status'=>'Berhasil'
            
        ];

        $pushData = $database->getReference("JemputSampah/{$email}/{$kode}")->update($data);


        return redirect()->action('JemputsampahController@requestjemput');

    }



    public function riwayatdatajemput(Request $request){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $kode = $request->ref;

        $database = $firebase->getDatabase();
        $db = $firebase->getDatabase();
        $jalur = "JemputSampah/{$kode}";

        // $ref =  $database->getReference("$jalur")->orderByChild('Status')->equalTo("Sedang diproses");
        $ref =  $database->getReference("$jalur");;
        // $stat = $database->getReference("AntarSampah/{$kode}/Status");
        
        $datajemput = $ref->getValue();

        $ambilemail =  $database->getReference("Users/{$kode}/email");
        $email= $ambilemail->getValue();
        
            // return json_encode($dataantar);
        //    return view('statusantar',compact('proses'));
        
        return view('riwayatjemputdata',compact('datajemput','email'));
    }
    public function riwayatjemput(){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();


        $ref2 =  $database->getReference("JemputSampah");
        $jemputsampah = $ref2->getValue();

            // return json_encode($all_subject);
          return view('riwayatjemput',compact('jemputsampah'));
          

    }



}
