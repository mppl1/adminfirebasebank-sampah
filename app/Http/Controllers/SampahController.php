<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;


class SampahController extends Controller
{
    public function sampah(){

        return view('sampah');
    }

    public function datasampah(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("Jenis_Sampah");
        
        $antarsampah = $ref->getValue();
        // foreach($antarsampah as $subject){
        //     $all_sampah[] = $subject;
        // }
        return view('datasampah',compact('antarsampah'));
    }


    public function addsampah(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();
        $database = $firebase->getDatabase();
        $ref =  $database->getReference("Jenis_Sampah");

        
        $jenissampah = $request->jenis_sampah;
        $satuan = $request->satuan;

        $key = $ref->push()->getKey();

        $ref->getChild($key)->set([
            'JenisSampah'=>$jenissampah, 
            'Satuan'=>$satuan,
            
        ]);


        return redirect()->action('SampahController@datasampah');

    }















    public function tessampah(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("Jenis_SampahTes");
        
        $antarsampah = $ref->getValue();
        // foreach($antarsampah as $subject){
        //     $all_sampah[] = $subject;
        // }
        return view('tessampah',compact('antarsampah'));
    }


    public function editsampah(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("Jenis_Sampah");
        
        $antarsampah = $ref->getValue();
        // foreach($antarsampah as $subject){
        //     $all_sampah[] = $subject;
        // }
        return view('editsampah',compact('antarsampah'));
    }



   

  



    public function  updatesampah(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
        ->create();
        $database = $firebase->getDatabase();
        $kode = $request->ref;
        // $ref =  $database->getReference("Jenis_Sampah/{$kode}");

        
        $jenissampah = $request->jenis_sampah;
        $satuan = $request->satuan;
       

        $data = [
            'JenisSampah'=>$jenissampah, 
            'Satuan'=>$satuan
        ];
        $pushData = $database->getReference("Jenis_Sampah/{$kode}")->update($data);

       


        return redirect()->action('SampahController@editsampah');
    }

    public function hapussampah(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
        ->create();
        $database = $firebase->getDatabase();
        $gaga = $request->kunci;
   
        $delData = $database->getReference("Jenis_Sampah/{$gaga}")->remove();
    
     return redirect()->action('SampahController@editsampah');

    }















    public function poin(){

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ambilpoin =  $database->getReference("UsersWeb/ivani@gmail_com/point");
        $poin = $ambilpoin->getValue();
       
        return view('poin',compact('poin'));
    }


}
