<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class EventController extends Controller
{
    public function event(){

        
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

    

        $ev =  $database->getReference("Event");
        $event = $ev->getValue();

        foreach($event as $subject){
            $all_event[] = $subject;
        }
        return view('event',compact('all_event'));
}

    public function tambahevent(){

        return view('tambahevent');
}

public function addtambahevent(Request $request){
      

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
    ->create();
    $database = $firebase->getDatabase();
    $ref =  $database->getReference("Event");

    
    $nama = $request->nama;
    $tanggal = $request->tanggal;
    $lokasi = $request->lokasi;
    $deskripsi = $request->deskripsi;

    $key = $ref->push()->getKey();

    $ref->getChild($key)->set([
        'namaEvent'=>$nama, 
        'waktuEvent'=>$tanggal,
        'tempatEvent'=>$lokasi,
        'descEvent'=>$deskripsi,
        'urlevent'=>"https://firebasestorage.googleapis.com/v0/b/bank-sampah-750d0.appspot.com/o/Event%2FDel%20Creativity.jpg?alt=media&token=c9770b45-79b6-4122-9a5e-79d4b2bf6966"
    ]);


    return redirect()->action('EventController@event');

}

public function editevent(){

        
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
    ->create();

    $database = $firebase->getDatabase();

    $ev =  $database->getReference("Event");
    $event = $ev->getValue();

    return view('editevent',compact('event'));
}


public function  updateevent(Request $request){
      

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
    ->create();
    $database = $firebase->getDatabase();
    $kode = $request->ref;
    // $ref =  $database->getReference("Jenis_Sampah/{$kode}");

    
    $nama = $request->nama;
    $tanggal = $request->tanggal;
    $lokasi = $request->lokasi;
    $deskripsi = $request->deskripsi;
   

    $data = [
        'namaEvent'=>$nama, 
        'waktuEvent'=>$tanggal,
        'tempatEvent'=>$lokasi,
        'descEvent'=>$deskripsi,
    ];
    $pushData = $database->getReference("Event/{$kode}")->update($data);

   


    return redirect()->action('EventController@event');
}


public function hapusevent(Request $request){
      

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
    ->create();
    $database = $firebase->getDatabase();
    $gaga = $request->kunci;

    $delData = $database->getReference("Event/{$gaga}")->remove();

 return redirect()->action('EventController@event');

}

}