<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class PelangganController extends Controller
{
    public function pelanggan(){    
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
   
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("Users");

        $pelanggan = $ref->getValue();

        foreach($pelanggan as $subject){
            $all_subject[] = $subject;
        }
        // return json_encode($all_subject);
          return view('pelanggan',compact('all_subject'));

    }

   


    public function updateprofil(Request $request){


        $this->nama_lengkap = $request->nama_lengkap;
 
        $ServiceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $Firebase = (new Factory)
            ->withServiceAccount($ServiceAccount)
            ->create();
        $database = $Firebase->getDatabase();


        $ref = $database->getReference("UsersWeb/senin2@gmail_com");
               
        //  $ref = $database->getReference('Users');
  
        $ref->update([
        // $ref->set([
            'nama_lengkap'=>$this->nama_lengkap,
         
        ]);
        
        return redirect()->action('PelangganController@pelanggan');

    }


    public function  datapelanggan(Request $request){  
       
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        // $kode = $request->user;

        $this->email = $request->user;
        $emailku = str_replace('.', '_',$this->email);

        $database = $firebase->getDatabase();
        $db = $firebase->getDatabase();

        $refnama =  $database->getReference("Users/{$emailku}/nama_lengkap");;
        $nama = $refnama->getValue();

        $refemail =  $database->getReference("Users/{$emailku}/email");;
        $email = $refemail->getValue();

        $reftelp=  $database->getReference("Users/{$emailku}/no_telp");;
        $telepon = $reftelp->getValue();


        $refkerja=  $database->getReference("Users/{$emailku}/pekerjaan");;
        $pekerjaan = $refkerja->getValue();

        $refidentitas=  $database->getReference("Users/{$emailku}/no_identitas");;
        $identitas = $refidentitas->getValue();

        $refimages=  $database->getReference("Users/{$emailku}/profile_image_url");;
        $images= $refimages->getValue();
        
        // return json_encode($nama);
        return view('datapelanggan',compact('nama','email','telepon','pekerjaan','identitas','images'));
    }


   

}
