<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class TerimaSampahController extends Controller
{
    public function terima(){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        // The following line is optional if the project id in your credentials file
        // is identical to the subdomain of your Firebase project. If you need it,
        // make sure to replace the URL with the URL of your project.
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("Jenis_Sampah");
        
        $antarsampah = $ref->getValue();
        foreach($antarsampah as $subject){
            $all_subject[] = $subject;
        }

        // return json_encode($all_subject);
        return view('terima',compact('all_subject'));
        // return view('terima');
    }

    public function addterima(Request $request){
      

        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)  
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();
        $database = $firebase->getDatabase();
        $this->email = $request->email;
        $emailku = str_replace('.', '_',$this->email);
        $ref =  $database->getReference("AntarSampah/{$emailku}");

        
        $jenissampah = $request->jenissampah;
        $berat  = $request->berat;
        $satuan = $request->satuan;
        $tanggal = $request->tanggal;
        $poin = $request->poin;
        $status = $request->status;

        $key = $ref->push()->getKey();

        $ref->getChild($key)->set([
            'jenisSampah'=>$jenissampah,
            'berat'=>$berat,
            'satuan'=>$satuan,
            'tanggal'=>$tanggal,
            'poin'=>$poin,
            'currentId'=>$emailku,
            'status'=>'Berhasil'
            
        ]);

        $subjects = $ref->getValue();

        
        $updatepoin = [
            'point'=>$poin
        ];
     
        $pushPoin = $database->getReference("Users/{$emailku}")->update($updatepoin);

        foreach($subjects as $subject){
            $all_subject[] = $subject;
        }



        return redirect()->action('TerimasampahController@terima');

    }
}
