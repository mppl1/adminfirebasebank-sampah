<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class FirebaseController extends Controller
{
    public function index(){
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->create();

            $database = $firebase->getDatabase();
        
            $ref = $database->getReference('AntarSampah');

            $key = $ref->push()->getKey();
            $ref->getChild($key)->set([
                'JenisSampah'=>'Sampah Mania',
                'Berat'=>'200',
                'Satuan'=>'Kg',
                'Tanggal'=>'2020/03/22',
                'Poin'=>'10500',
                'Status'=>'Sedang diproses'
                
            
            ]);

            return $key;
    

    }
}
