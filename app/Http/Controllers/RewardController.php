<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class RewardController extends Controller
{
public function reward(){

    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
    ->create();

    $database = $firebase->getDatabase();



    $re =  $database->getReference("Reward");
    $reward = $re->getValue();

    foreach($reward as $subject){
        $all_reward[] = $subject;
    }
    return view('reward',compact('all_reward'));
}

public function tambahreward(){
    return view('tambahreward');
}

public function addtambahreward(Request $request){ 
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
    ->create();
    $database = $firebase->getDatabase();
    $ref =  $database->getReference("Reward");

    $jenisReward = $request->jenisReward;
    $namaReward = $request->namaReward;
    $pointReward = $request->pointReward;

    $key = $ref->push()->getKey();

    $ref->getChild($key)->set([
        'jenisReward'=>$jenisReward,  
        'namaReward'=>$namaReward,
        'pointReward'=>$pointReward,
        'urlreward'=>"https://firebasestorage.googleapis.com/v0/b/bank-sampah-750d0.appspot.com/o/Reward%2FPin%20Del.jpg?alt=media&token=cb2a443f-8761-4938-90ae-1b3d7aaba6bc"
    ]);
    return redirect()->action('RewardController@reward');
}

public function editreward(){
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)
    // The following line is optional if the project id in your credentials file
    // is identical to the subdomain of your Firebase project. If you need it,
    // make sure to replace the URL with the URL of your project.
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
    ->create();

    $database = $firebase->getDatabase();

    $re =  $database->getReference("Reward");
    $reward = $re->getValue();

    return view('editreward',compact('reward'));
}


public function  updatereward(Request $request){
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
    ->create();
    $database = $firebase->getDatabase();
    $kode = $request->ref;
    // $ref =  $database->getReference("Jenis_Sampah/{$kode}");
    
    $jenisReward = $request->jenisReward;
    $namaReward = $request->namaReward;
    $pointReward = $request->pointReward;

    $data = [
        'jenisReward'=>$jenisReward,  
        'namaReward'=>$namaReward,
        'pointReward'=>$pointReward,
    ];
    $pushData = $database->getReference("Reward/{$kode}")->update($data);

    return redirect()->action('RewardController@reward');
}

public function hapusreward(Request $request){
    $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
    $firebase = (new Factory)
    ->withServiceAccount($serviceAccount)  
    ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')  
    ->create();
    $database = $firebase->getDatabase();
    $gaga = $request->kunci;

    $delData = $database->getReference("Reward/{$gaga}")->remove();

 return redirect()->action('RewardController@reward');

}

}
