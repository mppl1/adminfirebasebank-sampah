<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;


class KonfirmasiController extends Controller
{
    public function konfirmasi(){  
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();

        $ref =  $database->getReference("AntarSampahWeb");
        $snap = $ref->getSnapShot();
        if($snap->exists()){
            $users = $ref->getChildKeys();
            for($i=0; $i<count($users);$i++){
                $user = $users[$i];
                $arr = $database->getReference("AntarSampahWeb/$user")->getChildKeys();
                for($j=0; $j<count($arr);$j++){
                    $key = $arr[$j];
                    $tanggal = $database->getReference("AntarSampahWeb/$user/$key")->getValue();
                    $status = $database->getReference("AntarSampahWeb/$user/$key")->getValue();
                    $array2[$j] = array($key, $tanggal, $status);
                }
                $array[$i] = array($user, $array2);
            }
    
        }
    return json_encode($users);
    // return view('StatusAntar', compact('array2'));
    // return view('StatusAntar', compact('array'));  

    }
    public function index(){
        $request = $this->konfirmasi();
        $serviceAccount = ServiceAccount::fromJsonFile(__DIR__.'/FirebaseKey.json');
        
        $firebase = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->withDatabaseUri('https://bank-sampah-750d0.firebaseio.com/')
        ->create();

        $database = $firebase->getDatabase();
        if(!empty($request)){
            for($i=0; $i<count($request); $i++){
                $user = $request[$i][0];
                $nama[$i] = $database->getReference("UsersWeb/$user/nama_lengkap")->getValue();
            }
        }
        
         return view('StatusAntar', compact('request'));    
    }



}
